<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaPreguntaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'respuesta_pregunta';

    /**
     * Run the migrations.
     * @table respuesta_pregunta
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('respuesta_IDRespuesta');
            $table->integer('pregunta_IDPregunta');

            $table->index(["respuesta_IDRespuesta"], 'fk_respuesta_pregunta_respuesta1_idx');

            $table->index(["pregunta_IDPregunta"], 'fk_respuesta_pregunta_pregunta1_idx');


            $table->foreign('respuesta_IDRespuesta', 'fk_respuesta_pregunta_respuesta1_idx')
                ->references('IDRespuesta')->on('respuesta')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('pregunta_IDPregunta', 'fk_respuesta_pregunta_pregunta1_idx')
                ->references('IDPregunta')->on('pregunta')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
