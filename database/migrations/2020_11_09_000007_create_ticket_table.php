<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ticket';

    /**
     * Run the migrations.
     * @table ticket
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('IDTicket');
            $table->string('DescripcionTicket', 30);
            $table->dateTime('FechaRegistro');
            $table->dateTime('FechaAtendido')->nullable()->default(null);
            $table->integer('IDUsuario');
            $table->integer('IDTipo');
            $table->integer('departamento_IDDepartamento')->nullable();
            $table->integer('pregunta_IDPregunta');
            $table->integer('usuariogeneral_IDUsuario');
            $table->integer('estatusticket_IDEstatus');

            $table->index(["pregunta_IDPregunta"], 'fk_ticket_pregunta1_idx');

            $table->index(["estatusticket_IDEstatus"], 'fk_ticket_estatusticket1_idx');

            $table->index(["departamento_IDDepartamento"], 'fk_ticket_departamento1_idx');

            $table->index(["IDUsuario", "IDTipo"], 'Refusuariogeneral11');

            $table->index(["usuariogeneral_IDUsuario"], 'fk_ticket_usuariogeneral1_idx');


            $table->foreign('departamento_IDDepartamento', 'fk_ticket_departamento1_idx')
                ->references('IDDepartamento')->on('departamento')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('pregunta_IDPregunta', 'fk_ticket_pregunta1_idx')
                ->references('IDPregunta')->on('pregunta')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('usuariogeneral_IDUsuario', 'fk_ticket_usuariogeneral1_idx')
                ->references('IDUsuario')->on('usuariogeneral')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('estatusticket_IDEstatus', 'fk_ticket_estatusticket1_idx')
                ->references('IDEstatus')->on('estatusticket')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
