<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJefedepTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'jefedep';

    /**
     * Run the migrations.
     * @table jefedep
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('IDJefeD');
            $table->string('ContrasenaJefeD', 30);
            $table->string('MatriculaJefeD', 20);
            $table->char('NombreJefeD', 20);
            $table->char('APaternoJefeD', 20);
            $table->char('AMaternoJefeD', 20);
            $table->string('CorreoJefeD', 40);
            $table->integer('departamento_IDDepartamento');

            $table->index(["departamento_IDDepartamento"], 'fk_jefedep_departamento1_idx');


            $table->foreign('departamento_IDDepartamento', 'fk_jefedep_departamento1_idx')
                ->references('IDDepartamento')->on('departamento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
