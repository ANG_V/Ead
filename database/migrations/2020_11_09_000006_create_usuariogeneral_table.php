<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariogeneralTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'usuariogeneral';

    /**
     * Run the migrations.
     * @table usuariogeneral
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('IDUsuario');
            $table->string('ContrasenaUsuario', 30);
            $table->string('MatriculaUsuario', 20);
            $table->char('NombreUsuario', 20);
            $table->char('APaternoUsuario', 20);
            $table->char('AMaternoUsuario', 20);
            $table->string('CorreoUsuario', 40);
            $table->integer('tipousuario_IDTipo');

            $table->index(["tipousuario_IDTipo"], 'fk_usuariogeneral_tipousuario1_idx');


            $table->foreign('tipousuario_IDTipo', 'fk_usuariogeneral_tipousuario1_idx')
                ->references('IDTipo')->on('tipousuario')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
