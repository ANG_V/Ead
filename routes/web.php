<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Inicio*/
Route::get('/', function () {
    return view('welcome');
});
/*Inicio de sesión*/
Route::get('/login_2', function() {
	return view('Login.login_2');
});
/*Administrador*/
Route::resource('/administrador', 'AdminstradorController');

Route::get('/usuarios', function() {
	return view('Administrador.usuarios');
});

Route::get('/alumnos', function() {
	return view('Administrador.alumnos');
});

Route::get('/preguntas', function() {
	return view('Administrador.preguntas');
});

Route::get('/tickets', function() {
	return view('Administrador.tickets');
});

Route::get('/estados', function() {
	return view('Administrador.estados');
});

Route::get('/soluciones', function() {
	return view('Administrador.soluciones');
});

Route::get('/departamentos', function() {
	return view('Administrador.departamentos');
});

Route::get('/reg_alumnos', function() {
	return view('Administrador.reg_alumnos');
});

Route::get('/reg_dep', function() {
	return view('Administrador.reg_dep');
});

Route::get('/reg_preguntas', function() {
	return view('Administrador.reg_preguntas');
});

Route::get('/reg_tickets', function() {
	return view('Administrador.reg_tickets');
});

Route::get('/reg_estados', function() {
	return view('Administrador.reg_estados');
});

Route::get('/reg_soluciones', function() {
	return view('Administrador.reg_soluciones');
});
/* Usuarios*/ 
Route::get('/alumno',function(){
    return view('Alumno.alumno');
});

Route::get('/aspirante',function(){
    return view('Aspirante.aspirante');
});

Route::get('/docente',function(){
    return view('Docente.docente');
});

Route::get('/egresado',function(){
    return view('Egresado.egresado');
});

Route::get('/general',function(){
    return view('Publico.general');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
