<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ead2</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <style type="text/css">
            html, body{
                background-color: #F2F2F2;
                margin: 0px;
            }
            #btns{
                background-color: #056CF2;
                color: #F2F2F2;
                width: 100%;
            }
            #btns2{
                color: #051726; 
                font-size: 18px;
                float: center;
            }
            #btni{
                width: 550px;
                height: 45px;
                color: #F2F2F2;

                /*border-right-color: #F2F2F2;*/
            }
            #btnedit{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #btneli{
                background-color: red;
                color: #F2F2F2;
            }
            #btnagr{
                background-color: green;
                color: #F2F2F2;
                margin: 20px;
            }
            #titulos{
                font-size: 5vw;
                text-align: center;
                height: 10px;
            }
            
            #titulo{
                height: 20px;
            }
            #vl {
                border-left: 2px solid #051726;
                height: 90px;
                position: absolute;
                top: 5%;
            }
            
            #imgs{
              margin: 15px;
              width: 90%;
              height: 50%;
            }
            .header_1{
                background-color: #051726;
                height: 40px;
                width: 100%;
           }
           .header_2{
                background-color: #F2F2F2;
                height: 80px;
                width: 100%;
           }
           .header_3{
                background-color: #056CF2;
                height: 80px;
                width: 100%;
           }
           .header_4{
                background-color: #051726;
                height: 50px;
                width: 100%
           }
           .header_5{
            background-color: yellow;
            height: 70px;
            width: 100%;
           }
           #header_6{
            background-color: #051726;
            color: #F2F2F2;
           }
           .header_7{
            background-color: #051726;
            height: 10px;
            width: 100%;
           }
           .header_8{
            background-color: #F2f2f2;
            height: 80px;
            width: 100%;
           }
           #b{
                font-family: ;
                font-size: 20px;
                color: #F2F2F2;
            }
            #ma{
                font-family: ;
                font-size: 300px;
                color: #F2F2F2;
                text-align: center;
            }
            .p{
                font-family: ;
                font-size: 30px;
                color: #F2F2F2;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            img.L1{
                margin: 20px;
                width: 7%;    
            }
            img.L2{
                width: 8%;
                margin: 10px;
            }
            #L3{
                width: 100px;
                margin: 10px;
            }
            #L4{
                width: 120px;
                margin: 10px;
            }
            .tb1{
               width: 100%; 
               height: 130px;
            }
            .td1{
                
                width: 30%;
            }
            .td2{
                
                width: 40%;
            }
            .td3{
                
                width: 30%;
            }   
            #borde2{
                padding: 2%;
                border: 3px solid blue;
                border-radius: 30px;
                border-color: black;
            }
            #borde1{
                border: 1px solid blue;
                border-radius: 20px;
                border-color: black;
            }
            #mytable{
              background-color: #FFFF;
              border-color: #0D65D9;
            }
            #th1{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #mgb{
                width: 100%;
            }
            #menu{
                color: #F2F2F2;
                font-family: ;
            }
            .buttons {
              background: #051726;
              border: none;
              color: white;
              border-radius: 30px;
            }
            #info{
              font-size: 2vw;
            }
            #buttons {
              background: #056CF2;
              border: none;
              color: white;
            }
            #atendidos {
              background: #3DD973;
              border: none;
              font-size: 2vw;
              color: white;
            }
            #canalizados {
              background: #D92525;
              border: none;
              font-size: 2vw;
              color: white;
            }
            #pendientes{
              background: #F2D43D;
              border: none;
              font-size: 2vw;
              color: white;
            }
        </style>
    </head>
    <body>
        <div class="header">
            <div class="content">
                    <header class="header_2">
                        <div class="d-flex text-black">
                            <div class="p-2 w-100 ">
                                <div class="d-flex">
                                    <div class="p-2 flex-fill bg">
                                        <div class="p-2"><img src="img/EscudoITM.png" class="img-fluid" id="L3"></div>
                                    </div>
                                    <div class="p-2 flex-fill bg">
                                        <div class="p-2"><img src="img/Defenitive.png" class="img-fluid" id="L4"></div>
                                    </div>
                                    <div class="p-2 flex-fill bg">
                                        <p id="vl" ></p>
                                    </div>
                                    <div class="p-2 flex-fill">
                                        <p class="text-dark" id="borde1" heigth="3rem">
                                            <span id="titulos">Mesa de Ayuda</span></p>
                                    </div>
                                    <div class="p-2 flex-fill bg">
                                        <p id="vl" ></p>
                                    </div>
                                </div>
                            </div >
                            <div class="p-2 flex-shrink-1">
                                <div class="d-flex">
                                    <div class="p-2 flex-fill">
                                        <div class="p-2">
                                            <div class="p-2"><a href="{{url('/')}}">   
                                            <button class="btn" id="btns2">
                                                <svg width="1rem" height="1rem" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                                                    <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                                </svg>Home
                                            </button></a>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="p-2 flex-fill">
                                        <div class="p-2">
                                            <div class="p-2"><a href="{{url('/login_2')}}">   
                                                <button class="btn" id="btns2">
                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-door-closed-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M4 1a1 1 0 0 0-1 1v13H1.5a.5.5 0 0 0 0 1h13a.5.5 0 0 0 0-1H13V2a1 1 0 0 0-1-1H4zm2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                                    </svg>Cerrar sesión
                                                </button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div>&nbsp;</div> <div>&nbsp;</div> <div>&nbsp;</div> 
                    <div><header class="header_7"></header></div>
                    <header class="header_8"></header>
            </div>
        </div>
        <div class="head">
            
        </div>


        </div>
        <div class="body">
            <nav class="navbar navbar-expand-lg" id="buttons">
                    <a class="navbar-brand" href="{{url('/administrador')}}" id="menu">ADMINISTRADOR
                                    <span class="sr-only">(current)</span></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" id="buttons">
                    <span class="navbar-toggler-icon">---</span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto" id="menu">
                        <li class="nav-item" id="menu">
                            <div class="p-2">
                                <div class="d-flex justify-content-center">
                                    <a class="nav-item nav-link" href="{{url('/departamentos')}}" id="menu">
                                        DEPARTAMENTOS</a>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="p-2"><a class="nav-item nav-link" href="{{url('/usuarios')}}" id="menu">
                                    USUARIOS</a></div>
                        </li>
                        <li class="nav-item">
                        <div class="p-2"><a class="nav-item nav-link" href="{{url('/preguntas')}}" id="menu">
                                    PREGUNTAS</a></div>
                        </li>
                        <li class="nav-item">
                            <div class="p-2"><a class="nav-item nav-link" href="{{url('/soluciones')}}" id="menu">
                                    Soluciones</a></div>
                        </li>
                        <li class="nav-item">
                            <div class="p-2"><a class="nav-item nav-link" href="{{url('/tickets')}}" id="menu">
                                    TICKETS</a></div>
                        </li>
                        <li class="nav-item">
                            <div class="p-2"><a class="nav-item nav-link" href="{{url('/estados')}}" id="menu">
                                    ESTADO TICKETS</a></div>
                        </li>
                        <li class="nav-item">
                            <div class="p-2"><a class="nav-item nav-link" href="{{url('/soluciones_tickets')}}" id="menu">
                                    SOLUCIÓN TICKETS</a></div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="container-fluid">
                <div><header class="header_8"></header></div>
                <div class="row no-gutters">
                    
                    <div class="col-6 col-md-4" id="borde2">
                            <div class="d-flex justify-content-center" id="info">
                                <div class="p-2">Información</div>
                            </div>
                            <form>
                                <fieldset disabled>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Nombre</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="nombre">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Apelldo Paterno</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="paterno">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Apellido MAterno</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="materno">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Correo</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="123@qwe.asd">
                                    </div>
                                    <div class="form-group">
                                        <label for="disabledTextInput">Matricula</label>
                                        <input type="text" id="disabledTextInput" class="form-control" placeholder="matricula tec">
                                    </div>
                                </fieldset>
                                <div class="d-flex justify-content-center">
                                    <div class="p-2"><p><button type="button" class="btn btn-link" >¿Olvidastes tu Contraseña?</button></p></div>
                                </div>
                            </form>
                    </div>
                    <div class="col-12 col-sm-6 col-md-8">
                        <div class="d-flex justify-content-center"  id="buttons">
                            <div class="p-2">Estado de Tickets</div>
                        </div>
                        <div class="d-flex justify-content-around" id="menu">
                            <div class="p-2"  id="atendidos">Atendidos<br>
                                    <div class="d-flex justify-content-center">
                                        <div class="p-2">N</div>
                                    </div>
                            </div>
                            <div class="p-2"  id="pendientes">Pendientes<br>
                                    <div class="d-flex justify-content-center">
                                        <div class="p-2">N</div>
                                    </div>
                            </div>
                            <div class="p-2"  id="canalizados">Canalizados<br>
                                    <div class="d-flex justify-content-center">
                                        <div class="p-2">N</div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        <!--scripts-->
        <script type="text/javascript" src="js/jquery-3.5.0.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    </body>
</html>
