<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ead2</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <!-- Styles -->
        <style type="text/css">
             html, body{
                background-color: #F2F2F2;
                font-weight: 100;
                height: 100vh;
                margin: 0px;
            }
            #btns{
                background-color: #056CF2;
                color: #F2F2F2;
                width: 100%;
            }
            #btns2{
                color: #051726; 
                width: 120px;
                float: right;
            }
            #btni{
                width: 550px;
                height: 45px;
                color: #F2F2F2;

                /*border-right-color: #F2F2F2;*/
            }
            #btnedit{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #btneli{
                background-color: red;
                color: #F2F2F2;
            }
            #btnagr{
                background-color: green;
                color: #F2F2F2;
                margin: 20px;
            }
            #imgs{
              margin: 15px;
              width: 90%;
              height: 50%;
            }
            .header_1{
                background-color: #051726;
                height: 40px;
                width: 100%;
           }
           .header_2{
                background-color: #F2F2F2;
                height: 130px;
                width: 100%;
           }
           .header_3{
                background-color: #056CF2;
                height: 80px;
                width: 100%;
           }
           .header_4{
                background-color: #0D65D9;
                height: 45px;
                width: 100%
           }
           .header_5{
            background-color: yellow;
            height: 70px;
            width: 100%;
           }
           .header_6{
            background-color: #051726;
            color: #F2F2F2;
            height: 350px;
            width: 100%;
           }
           .header_7{
            background-color: #051726;
            height: 10px;
            width: 100%;
           }
           .header_8{
            background-color: #FFFF;
            height: 80px;
            width: 100%;
           }
           #b{
                font-family: ;
                font-size: 20px;
                color: #F2F2F2;
            }
            #ma{
                font-family: ;
                font-size: 300px;
                color: #F2F2F2;
                text-align: center;
            }
            .p{
                font-family: ;
                font-size: 30px;
                color: #F2F2F2;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            img.L1{
                margin: 20px;
                width: 7%;    
            }
            img.L2{
                width: 8%;
                margin: 10px;
            }
            #L3{
                width: 100px;
                margin: 10px;
            }
            #L4{
                width: 120px;
                margin: 10px;
            }
            .tb1{
               width: 100%; 
               height: 130px;
            }
            .td1{
                
                width: 30%;
            }
            .td2{
                
                width: 40%;
            }
            .td3{
                
                width: 30%;
            }   
            #mytable{
              background-color: #FFFF;
              border-color: #0D65D9;
            }
            #th1{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #mgb{
                width: 100%;
            }
            #menu{
                color: #F2F2F2;
                font-family: ;
            }
        </style>
    </head>
    <body>
        <div class="head">
            <div class="content">
                <div>
                    <header class="header_1"></header>
                </div>
                    <header class="header_2">
                        <table class="table"> 
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="img/EscudoITM.png" class="img-fluid" id="L3"> 
                                        <img src="img/Defenitive.png" class="img-fluid" id="L4">
                                        <!--<img src="EscudoITM.png" alt="..." class="img-thumbnail" id="L3">
                                        <img src="Defenitive.png" alt="..." class="img-thumbnail" id="L4">-->
                                    </td>
                                    <td>
                                        <p class="ma" style="font-size: 70px; text-align: center">MESA DE AYUDA</p>
                                    </td>
                                    <td>
                                        <a href="{{url('/')}}">   
                                            <button class="btn" id="btns2">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-door-closed-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4 1a1 1 0 0 0-1 1v13H1.5a.5.5 0 0 0 0 1h13a.5.5 0 0 0 0-1H13V2a1 1 0 0 0-1-1H4zm2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                                </svg>Cerrar sesión
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}">   
                                        <button class="btn" id="btns2">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                                                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                            </svg> Home
                                        </button>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </header>
                    <header class="header_7"></header>
                    <header class="header_8">
                        <p style="text-align: center; font-size: 40px; color: #051726;">ALUMNOS</p>
                    </header>
                    <nav class="navbar navbar-expand-lg navbar bg" style="background-color: #0D65D9;">
                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                <a class="nav-item nav-link active" id="btni" href="{{url('/administrador')}}">ADMINISTRADOR
                                    <span class="sr-only">(current)</span>
                                </a>
                                <a class="nav-item nav-link" href="{{url('/departamentos')}}" id="menu">DEPARTAMENTOS</a>
                                <a class="nav-item nav-link" href="{{url('/usuarios')}}" id="menu">USUARIOS</a>
                                <a class="nav-item nav-link" href="{{url('/preguntas')}}" id="menu">PREGUNTAS</a>
                                <a class="nav-item nav-link" href="{{url('/tickets')}}" id="menu">TICKETS</a>
                                <a class="nav-item nav-link" href="{{url('/estados')}}" id="menu">ESTADO TICKETS</a>
                                <a class="nav-item nav-link" href="{{url('/soluciones')}}" id="menu">SOLUCIÓN TICKETS</a>
                            </div>
                        </div>
                    </nav>
                    <header class="header_8">
                        <a href="{{url('/reg_alumnos')}}">
                            <button class="btn" id="btnagr">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3v-3z"/>
                                </svg>  Registrar
                            </button>
                        </a>
                    </header>
            </div>
        </div>
        <div class="body">
            <div class="container-fluid">
                <table class="table display highlight" cellspacing="0" width="90%" id="mytable" >
                    <thead class="thead" id="th1">
                        <tr>
                            <th>IDUsuario</th>
                            <th>IDTipo</th>
                            <th>NOMBRE</th>
                            <th>PATERNO</th>
                            <th>MATERNO</th>
                            <th>MATRÍCULA</th>
                            <th>CORREO ELECTRÓNICO</th>
                            <th>CONTRASEÑA</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--enlace con la bdd-->
                        <tr id="">
                            <td>1224</td>
                            <td>1224</td>
                            <td>Jose Guadalupe Timoteo </td>
                            <td>Gonzalez </td>
                            <td>Hernandez</td>
                            <td>123456</td>
                            <td>dcnwidbcwi@morelia.tecnm.mx</td>
                            <td>123456</td>
                            <td>
                                <a href="">
                                    <button class="btn" id="btnedit">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </button>
                                </a>
                                <a href="">
                                    <button class="btn" id="btneli">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                        </svg>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!--registrar-->
            <!--<div class="modal" id="ventanaModal" tabindex="-1" role="dialog" aria-labelledby="tituloVentana" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 id="tituloVentana">Registrar Alumnos</h5>
                            <button class="close" data-dismiss="modal" arial-label="Cerrar">
                                <span arial-hidden="true"></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!--<div class="alert alert-success">
                                <h6><strong>bfeksrvjk</strong></h6>
                            </div>-->
                            <!--<form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nombre</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Paterno</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Materno</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Matrícula</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Correo Electrónico Institucional</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Contraseña</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1">
                                </div>
                                  <button type="submit" class="btn btn-primary">Registrar</button>
                            </form> 
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" type="button" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="footer"></div>

        <!--scripts-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
