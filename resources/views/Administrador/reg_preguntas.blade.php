<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ead2</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <!-- Styles -->
        <style type="text/css">
             html, body{
                background-color: #F2F2F2;
                font-weight: 100;
                height: 100vh;
                margin: 0px;
            }
            #btns{
                background-color: #056CF2;
                color: #F2F2F2;
                width: 100%;
            }
            #btns2{
                color: #051726; 
                width: 120px;
                float: right;
            }
            #btni{
                width: 550px;
                height: 45px;
                color: #F2F2F2;

                /*border-right-color: #F2F2F2;*/
            }
            #btnedit{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #btneli{
                background-color: red;
                color: #F2F2F2;
            }
            #btnagr{
                background-color: green;
                color: #F2F2F2;
                margin: 20px;
            }
            #btnreg{
                background-color: #0D65D9;
                color: #F2F2F2;
                margin: 20px;
            }
            #imgs{
              margin: 15px;
              width: 90%;
              height: 50%;
            }
            .header_1{
                background-color: #051726;
                height: 40px;
                width: 100%;
           }
           .header_2{
                background-color: #F2F2F2;
                height: 130px;
                width: 100%;
           }
           .header_3{
                background-color: #056CF2;
                height: 80px;
                width: 100%;
           }
           .header_4{
                background-color: #0D65D9;
                height: 45px;
                width: 100%
           }
           .header_5{
            background-color: yellow;
            height: 70px;
            width: 100%;
           }
           .header_6{
            background-color: #051726;
            color: #F2F2F2;
            height: 350px;
            width: 100%;
           }
           .header_7{
            background-color: #051726;
            height: 10px;
            width: 100%;
           }
           .header_8{
            background-color: #FFFF;
            height: 80px;
            width: 100%;
           }
           .hreg{
            width: 850px;
            height: 50px;
            background-color: #051726;
            color: #F2F2F2;
            font-family: ;
            font-size: 20px;
           }
           .hpr1{
            width: 850px;
            height: 10px;
            background-color: #0D65D9;
           }
           .hpr{
            width: 130px;
           }
           #b{
                font-family: ;
                font-size: 20px;
                color: #F2F2F2;
            }
            #ma{
                font-family: ;
                font-size: 300px;
                color: #F2F2F2;
                text-align: center;
            }
            .p{
                font-family: ;
                font-size: 30px;
                color: #F2F2F2;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            img.L1{
                margin: 20px;
                width: 7%;    
            }
            img.L2{
                width: 8%;
                margin: 10px;
            }
            #L3{
                width: 100px;
                margin: 10px;
            }
            #L4{
                width: 120px;
                margin: 10px;
            }
            .tb1{
               width: 100%; 
               height: 130px;
            }
            .td1{
                
                width: 30%;
            }
            .td2{
                
                width: 40%;
            }
            .td3{
                
                width: 30%;
            }   
            #mytable{
              background-color: #FFFF;
              border-color: #0D65D9;
            }
            #th1{
                background-color: #0D65D9;
                color: #F2F2F2;
            }
            #mgb{
                width: 100%;
            }
            #menu{
                color: #F2F2F2;
                font-family: ;
            }
            .form-reg {
              width: 700px;
              height: 530px;
              background: ;
              margin: auto;
              padding: 50px 30px;
              /*border-top: 2px solid #051726;
              border-left: 2px solid #051726;
              border-right: 2px solid #051726;
              border-bottom: 2px solid #051726;*/
              color: #051726;
              font-family: ;
            }

            .form-reg p{
              height: 40px;
              text-align: center;
              border-bottom: 1px solid;
            }

            .form-reg a {
              color: white;
              text-decoration: none;
              font-size: 14px;
            }

            .form-reg a:hover {
              text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="head">
            <div class="content">
                <div>
                    <header class="header_1"></header>
                </div>
                    <header class="header_2">
                        <table class="table"> 
                            <tbody>
                                <tr>
                                    <td>
                                        <img src="img/EscudoITM.png" class="img-fluid" id="L3"> 
                                        <img src="img/Defenitive.png" class="img-fluid" id="L4">
                                        <!--<img src="EscudoITM.png" alt="..." class="img-thumbnail" id="L3">
                                        <img src="Defenitive.png" alt="..." class="img-thumbnail" id="L4">-->
                                    </td>
                                    <td>
                                        <p class="ma" style="font-size: 70px; text-align: center">MESA DE AYUDA</p>
                                    </td>
                                    <td>
                                        <a href="{{url('/')}}">   
                                            <button class="btn" id="btns2">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-door-closed-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M4 1a1 1 0 0 0-1 1v13H1.5a.5.5 0 0 0 0 1h13a.5.5 0 0 0 0-1H13V2a1 1 0 0 0-1-1H4zm2 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                                </svg>Cerrar sesión
                                            </button>
                                        </a>
                                        <a href="{{url('/')}}">   
                                        <button class="btn" id="btns2">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                                                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                            </svg> Home
                                        </button>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </header>
                    <header class="header_7"></header>
                    <header class="header_8">
                        <p style="text-align: center; font-size: 40px; color: #051726;"> PREGUNTAS</p>
                    </header>
                    <nav class="navbar navbar-expand-lg navbar bg" style="background-color: #0D65D9;">
                        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div class="navbar-nav">
                                <a class="nav-item nav-link active" id="btni" href="{{url('/administrador')}}">ADMINISTRADOR
                                    <span class="sr-only">(current)</span>
                                </a>
                                <a class="nav-item nav-link" href="{{url('/departamentos')}}" id="menu">DEPARTAMENTOS</a>
                                <a class="nav-item nav-link" href="{{url('/usuarios')}}" id="menu">USUARIOS</a>
                                <a class="nav-item nav-link" href="{{url('/preguntas')}}" id="menu">PREGUNTAS</a>
                                <a class="nav-item nav-link" href="{{url('/tickets')}}" id="menu">TICKETS</a>
                                <a class="nav-item nav-link" href="{{url('/estados')}}" id="menu">ESTADO DE LOS TICKETS</a>
                                <a class="nav-item nav-link" href="{{url('/soluciones')}}" id="menu">SOLUCIÓN DE LOS TICKETS</a>
                            </div>
                        </div>
                    </nav>
                    <header class="header_8">
                        <a href="{{url('/preguntas')}}">
                            <button class="btn" id="btnreg" >
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                                </svg>  Regresar
                            </button>
                        </a>
                    </header>
            </div>
        </div>
        <div class="body">
            <div class="container">
                <table>
                    <tr>
                        <td>
                            <header class="hpr"></header>
                        </td>
                        <td>
                            <header class="hreg">  
                                <svg width="3em" height="2sem" viewBox="0 0 16 16" class="bi bi-person-plus-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7.5-3a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                </svg> Registrar preguntas y respuestas
                            </header>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td><header class="hpr"></header></td>
                        <td><header class="hpr1"></header></td>
                    </tr>
                </table>
                <div class="form-reg">
                    <form class="registrar">
                        <div class="form-group" style="height: 50px">
                            <label for="exampleInputnom">
                                Descripción pregunta:
                            </label>
                                <input type="nom" class="form-control" id="exampleInputnom" aria-describedby="nomHelp">
                        </div>
                        <div class="form-group" style="height: 50px">
                            <label for="exampleInputPassword1">Descripción respuesta:</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                        </div><br><br>
                        <br><br><br><div>
                             <button type="submit" class="btn" style="background-color: #0D65D9; color: #F2F2F2; width: 100%">Registrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            
        </div>
        <div class="footer"></div>

        <!--scripts-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
