<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        html, body{
            background-color: #F2F2F2;
            margin: 0px;
        }
        #ini{
            background-color: #F2F2F2;
        }
        #btns{
            background-color: #056CF2;
            color: #F2F2F2;
            width: 100%;
        }
        #btns2{
            color: #051726; 
            font-size: 18px;
            float: center;
        }
        #btn3{
            float: center;
            width: 20%;
        }
        #imgs{
          margin: 15px;
          width: 90%;
          height: 50%;
        }
        #titulos{
            font-size: 5vw;
            text-align: center;
            height: 10px;
        }
        
        #titulo{
            height: 20px;
        }
        #vl {
            border-left: 2px solid #051726;
            height: 90px;
            position: absolute;
            top: 5%;
        }
        #collapseExample{
            background-color: #051726;
        }
        .header_1{
            background-color: #051726;
            height: 40px;
            width: 100%;
       }
       .header_2{
            background-color: #F2F2F2;
            height: 80px;
            width: 100%;
       }
       .header_3{
            background-color: #056CF2;
            height: 80px;
            width: 100%;
       }
       .header_4{
            background-color: #051726;
            height: 50px;
            width: 100%
       }
       .header_5{
        background-color: yellow;
        height: 70px;
        width: 100%;
       }
       #header_6{
        background-color: #051726;
        color: #F2F2F2;
       }
       .header_7{
        background-color: #051726;
        height: 4px;
        width: 100%;
       }
       .header_8{
        background-color: #051726;
        height: 50px;
        width: 100%;
       }
       #b{
            font-family: ;
            font-size: 20px;
            color: #F2F2F2;
        }
        #ma{
            font-family: ;
            font-size: 300px;
            color: #F2F2F2;
            text-align: center;
        }
        .p{
            font-family: ;
            font-size: 30px;
            color: #F2F2F2;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        img.L1{
            margin: 20px;
            width: 7%;    
        }
        img.L2{
            width: 8%;
            margin: 10px;
        }
        #L3{
            width: 100px;
            margin: 10px;
        }
        #L4{
            width: 120px;
            margin: 10px;
        }
        .tb1{
           width: 100%; 
           height: 130px;
        }
        .td1{
            
            width: 30%;
        }
        .td2{
            
            width: 40%;
        }
        .td3{
            
            width: 30%;
        }
        #borde1{
            border: 3px solid blue;
            border-radius: 30px;
            border-color:#056CF2;
        }

       .form-login {
          width: 600px;
          height: 350px;
          background: #F2F2F2;
          margin: auto;
          
          padding: 50px 30px;
          border-top: 4px solid #F2F2F2;
          color: white;
        }

        .form-login p{
          height: 40px;
          text-align: center;
          border-bottom: 1px solid;
        }

        .form-login a {
          color: white;
          text-decoration: none;
          font-size: 14px;
        }

        .form-login a:hover {
          text-decoration: underline;
        }

        .controls {
          width: 100%;
          border: 1px solid #056CF2;
          margin-bottom: 15px;
          padding: 11px 10px;
          background: #F2F2F2;
          font-size: 14px;
          font-family: ;
        }

        .buttons {
          background: #051726;
          border: none;
          color: white;
          border-radius: 30px;
        }
        #buttons {
          background: #056CF2;
          border: none;
          color: white;
          border-radius: 30px;
        }
        #black{
            color: black;
        }
        #face{
          width: 60px;
          height: 60px;
        }
        #twit{
          width: 60px;
          height: 60px;
        }
        #insta{
          width: 60px;
          height: 60px;
        }
        #yout{
          width: 60px;
          height: 60px;
        }
        .column {
            float: left;
            width: 33.33%;
            padding: 50px;
            text-align: center;
            font-size: 25px;
            cursor: pointer;
            color: white;
        }

        .containerTab {
            padding: 20px;
            color: white;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Closable button inside the image */
        .closebtn {
            float: right;
            color: white;
            font-size: 35px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="header">
        <div class="content">
                <header class="header_2">
                    <div class="d-flex text-black">
                        <div class="p-2 w-100 ">
                            <div class="d-flex">
                                <div class="p-2 flex-fill bg">
                                    <div class="p-2"><img src="img/EscudoITM.png" class="img-fluid" id="L3"></div>
                                </div>
                                <div class="p-2 flex-fill bg">
                                    <div class="p-2"><img src="img/Defenitive.png" class="img-fluid" id="L4"></div>
                                </div>
                                <div class="p-2 flex-fill bg">
                                    <p id="vl" ></p>
                                </div>
                                <div class="p-2 flex-fill">
                                    <p class="text-dark" id="borde1" heigth="3rem">
                                        <span id="titulos">Mesa de Ayuda</span></p>
                                </div>
                                <div class="p-2 flex-fill bg">
                                    <p id="vl" ></p>
                                </div>
                            </div>
                        </div >
                        <div class="p-2 flex-shrink-1">
                            <div class="d-flex">
                                <div class="p-2 flex-fill">
                                    <div class="p-2">
                                        <div class="p-2"><a href="{{url('/')}}">   
                                        <button class="btn" id="btns2">
                                            <svg width="1rem" height="1rem" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                                                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                            </svg>Home
                                        </button></a>
                                        </div>
                                    </div>    
                                </div>
                                <div class="p-2 flex-fill">
                                    <div class="p-2">
                                        <div class="p-2"><a href="{{url('/login')}}">   
                                            <button class="btn" id="btns2">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                                </svg>Entrar
                                            </button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div>&nbsp;</div> <div>&nbsp;</div> <div>&nbsp;</div> 
                <div><header class="header_7"></header></div>
        </div>
    </div>
    
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <content>
        <div class="d-flex text-white" id="header_6">
            <div class="p-2 flex-fill " id="header_6">
                <p class="p1"><FONT SIZE=4><strong>Campus I:</strong></FONT></p>
                <p class="p2"><FONT SIZE=3>Avenida Tecnológico #1500, Col. Lomas de Santiaguito. Morelia, Mich.</FONT></p>
                <p class="p3"><FONT SIZE=4><strong>Campus II:</strong></FONT></p>
                <p class="p4"><FONT SIZE=3>Camino de la Arboleda S/N, Residencial San Jose de la Huerta, Tenencia Morelos. Morelia, Mich.</FONT></p>
                <p class="p5"><FONT SIZE=4><strong>Contacto:</strong></FONT></p>
                <p class="p6"><FONT SIZE=3>Email: difusion@itmorelia.edu.mx Teléfono: +52 (443) 312 1570</FONT></p>
            </div>
            <div class="p-2 flex-fill " id="header_6">
                <a href="https://www.facebook.com/ITMoreliaOficial" target="_blank"><img src="img/Facebook.png" class="card-img-top" id="face"></a>
                <a href="https://twitter.com/itmoficial" target="_blank"><img src="img/Twitter.png" class="card-img-top" id="twit"></a><br>
                <a href="https://www.instagram.com/tecnm_campus_morelia/" target="_blank"><img src="img/Instagram.png" class="card-img-top" id="insta"></a>
                <a href="https://www.youtube.com/user/ITMoreliaOficial" target="_blank"><img src="img/Youtube.png" class="card-img-top" id="yout"></a>
            </div>
            <div class="p-2 flex-fill " id="header_6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15030.248876837308!2d-101.2269745535765!3d19.64600531701543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0c5f5c07b35d%3A0x8a5d3a9600947cb9!2sTecNM%20Morelia%20-%20Campus%202!5e0!3m2!1ses-419!2smx!4v1605160185673!5m2!1ses-419!2smx"
                 width="600" height="330" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" id="maps"></iframe>
            </div>
          </div>
    </content>
</body>
</html>
