@extends('layouts.app')

@section('content')
<style type="text/css">
    html, body{
        background-color: #F2F2F2;
        margin: 0px;
    }
    #ini{
        background-color: #F2F2F2;
    }
    #btns{
        background-color: #056CF2;
        color: #F2F2F2;
        width: 100%;
    }
    #btns2{
        color: #051726; 
        font-size: 18px;
        float: center;
    }
    #btn3{
        float: center;
        width: 20%;
    }
    #imgs{
      margin: 15px;
      width: 90%;
      height: 50%;
    }
    #titulos{
        font-size: 5vw;
        text-align: center;
        height: 10px;
    }
    
    #titulo{
        height: 20px;
    }
    #vl {
        border-left: 2px solid #051726;
        height: 90px;
        position: absolute;
        top: 5%;
    }
    #collapseExample{
        background-color: #051726;
    }
    .header_1{
        background-color: #051726;
        height: 40px;
        width: 100%;
   }
   .header_2{
        background-color: #F2F2F2;
        height: 80px;
        width: 100%;
   }
   .header_3{
        background-color: #056CF2;
        height: 80px;
        width: 100%;
   }
   .header_4{
        background-color: #051726;
        height: 50px;
        width: 100%
   }
   .header_5{
    background-color: yellow;
    height: 70px;
    width: 100%;
   }
   #header_6{
    background-color: #051726;
    color: #F2F2F2;
   }
   .header_7{
    background-color: #051726;
    height: 4px;
    width: 100%;
   }
   .header_8{
    background-color: #051726;
    height: 50px;
    width: 100%;
   }
   #b{
        font-family: ;
        font-size: 20px;
        color: #F2F2F2;
    }
    #ma{
        font-family: ;
        font-size: 300px;
        color: #F2F2F2;
        text-align: center;
    }
    .p{
        font-family: ;
        font-size: 30px;
        color: #F2F2F2;
    }
    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }
    .position-ref {
        position: relative;
    }
    img.L1{
        margin: 20px;
        width: 7%;    
    }
    img.L2{
        width: 8%;
        margin: 10px;
    }
    #L3{
        width: 100px;
        margin: 10px;
    }
    #L4{
        width: 120px;
        margin: 10px;
    }
    .tb1{
       width: 100%; 
       height: 130px;
    }
    .td1{
        
        width: 30%;
    }
    .td2{
        
        width: 40%;
    }
    .td3{
        
        width: 30%;
    }
    #borde1{
        border: 3px solid blue;
        border-radius: 30px;
        border-color:#056CF2;
    }

   .form-login {
      width: 600px;
      height: 350px;
      background: #F2F2F2;
      margin: auto;
      
      padding: 50px 30px;
      border-top: 4px solid #F2F2F2;
      color: white;
    }

    .form-login p{
      height: 40px;
      text-align: center;
      border-bottom: 1px solid;
    }

    .form-login a {
      color: white;
      text-decoration: none;
      font-size: 14px;
    }

    .form-login a:hover {
      text-decoration: underline;
    }

    .controls {
      width: 100%;
      border: 1px solid #056CF2;
      margin-bottom: 15px;
      padding: 11px 10px;
      background: #F2F2F2;
      font-size: 14px;
      font-family: ;
    }

    .buttons {
      background: #051726;
      border: none;
      color: white;
      border-radius: 30px;
    }
    #buttons {
      background: #056CF2;
      border: none;
      color: white;
      border-radius: 30px;
    }
    #black{
        color: black;
    }
    #face{
      width: 60px;
      height: 60px;
    }
    #twit{
      width: 60px;
      height: 60px;
    }
    #insta{
      width: 60px;
      height: 60px;
    }
    #yout{
      width: 60px;
      height: 60px;
    }
    .column {
        float: left;
        width: 33.33%;
        padding: 50px;
        text-align: center;
        font-size: 25px;
        cursor: pointer;
        color: white;
    }

    .containerTab {
        padding: 20px;
        color: white;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Closable button inside the image */
    .closebtn {
        float: right;
        color: white;
        font-size: 35px;
        cursor: pointer;
    }
</style>

<div class="header">
        <div class="content">
                    <header class="header_4">
                        <div class="flex-center">
                            <p class="p">¿Comó podemos ayudarte?</p>
                        </div>
                    </header>

                    
            </div>
        </div>
        
            
        <div class="body">
            <div class="container">
                <div class="col m2 s12 tema">
                    <div class="card-group">
                        <div class="card">
                                <img src="img/prueba.jpg" class="card-img-top" id="imgs" alt="...">
                            <div class="card-body"></div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="{{url('/')}}">
                                        <button type="button" class="btn" id="btns">ASPIRANTE</button>
                                    </a>
                                </small>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/prueba.jpg" class="card-img-top" id="imgs" alt="...">
                            <div class="card-body"></div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="{{url('/')}}">
                                        <button class="btn" id="btns" >ALUMNO</button>
                                    </a>
                                </small>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/prueba.jpg" class="card-img-top" id="imgs" alt="...">
                            <div class="card-body"></div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="{{url('/')}}">
                                        <button class="btn" id="btns">EGRESADO</button>
                                    </a>
                                </small>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/prueba.jpg" class="card-img-top" id="imgs" alt="...">
                            <div class="card-body"></div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="{{url('/')}}">
                                        <button class="btn" id="btns">DOCENTE</button>
                                    </a>
                                </small>
                            </div>
                        </div>
                        <div class="card">
                                <img src="img/prueba.jpg" class="card-img-top" id="imgs" alt="...">
                            <div class="card-body"></div>
                            <div class="card-footer">
                                <small class="text-muted">
                                    <a href="{{url('/')}}">
                                        <button class="btn" id="btns">PUBLICO</button>
                                    </a>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection
